package mta.k27.watermarking.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.k27.watermarking.R;

import java.util.ArrayList;

import mta.k27.watermarking.util.lsb.EvalutionUtils;
import mta.k27.watermarking.util.lsb.FileUtils;

import static android.content.ContentValues.TAG;
import static mta.k27.watermarking.activity.ImageActivity.IMAGE_HEIGHT;
import static mta.k27.watermarking.activity.ImageActivity.IMAGE_NAME;
import static mta.k27.watermarking.activity.ImageActivity.IMAGE_WIDTH;
import static mta.k27.watermarking.activity.ImageActivity.dataImg;
import static mta.k27.watermarking.activity.ImageActivity.dataImgEncoded;
import static mta.k27.watermarking.activity.ImageActivity.dataSig;

public class ImageDCTActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);
        init();
    }

    private void init() {
        Intent intent = getIntent();
        final String displayPath = intent.getStringExtra(EncodeActivity.EXTRA_FILE_TAG);
        final ImageView imageView = (ImageView) findViewById(R.id.imageView);
        new AsyncTask<Void, Void, Bitmap>() {
            @Override
            protected Bitmap doInBackground(Void... voids) {
                return FileUtils.decodeBitmapScaledDown(ImageDCTActivity.this, displayPath);
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                super.onPostExecute(bitmap);
                imageView.setImageBitmap(bitmap);
            }
        }.execute();

        try {
            int cols = intent.getIntExtra(IMAGE_WIDTH, 0);
            int rows = intent.getIntExtra(IMAGE_HEIGHT, 0);
            String name = intent.getStringExtra(IMAGE_NAME);

            FileUtils.saveData(this, name, dataSig);
            double mse = EvalutionUtils.getMse(cols, rows, dataImg, dataImgEncoded) / 3; // :3 because only red change, green & blue not change
            ((TextView) findViewById(R.id.tv_mse)).setText("Time = " + (System.currentTimeMillis() - MainActivity.startTime) / 1000);
            ((TextView) findViewById(R.id.tv_psnr)).setText("PSNR = " + EvalutionUtils.getPsnr(mse));
            ((TextView) findViewById(R.id.tv_ncc)).setText("NCC = " + EvalutionUtils.getNCC(dataImg, dataImgEncoded));
            Log.e(TAG, "MSE = " + mse);
            Log.e(TAG, "PSNR = " + EvalutionUtils.getPsnr(mse));
            Log.e(TAG, "NCC = " + EvalutionUtils.getNCC(dataImg, dataImgEncoded));
        } catch (Exception e) {
            Log.e("ImageActivity", "Exception: " + e.toString());
            e.printStackTrace();
        }

    }

}
