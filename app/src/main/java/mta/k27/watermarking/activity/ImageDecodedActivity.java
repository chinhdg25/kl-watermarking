package mta.k27.watermarking.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.k27.watermarking.R;

import mta.k27.watermarking.util.lsb.DecoderTask;
import mta.k27.watermarking.util.lsb.EvalutionUtils;
import mta.k27.watermarking.util.lsb.FileUtils;


public class ImageDecodedActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_decoded);
        init();
    }

    private void init() {
        try {
            Intent intent = getIntent();
            String displayPath = intent.getStringExtra(EncodeActivity.EXTRA_FILE_TAG);
            ImageView imageView = (ImageView) findViewById(R.id.imageView);
            imageView.setImageBitmap(FileUtils.decodeBitmapScaledDown(this, displayPath));
            int cols = (int) intent.getLongExtra(ImageActivity.IMAGE_WIDTH, 0);
            int rows = (int) intent.getLongExtra(ImageActivity.IMAGE_HEIGHT, 0);
            Log.e("ImageDecodedActivity", rows + " rows- cols = " + cols);
            double mse = EvalutionUtils.getMse(cols, rows, DecoderTask.dataSig, DecoderTask.dataDecodedSig) / 3;
            ((TextView) findViewById(R.id.tv_mse)).setText("Time = " + (double) (System.currentTimeMillis() - MainActivity.startTime) / 1000);
            ((TextView) findViewById(R.id.tv_psnr)).setText("PSNR = " + EvalutionUtils.getPsnr(mse));
            ((TextView) findViewById(R.id.tv_ncc)).setText("NCC = " + EvalutionUtils.getNCC(DecoderTask.dataSig, DecoderTask.dataDecodedSig));
        } catch (Exception e) {
        }
    }
}
