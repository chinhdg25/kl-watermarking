package mta.k27.watermarking.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.k27.watermarking.R;
import com.software.shell.fab.ActionButton;

import java.io.File;

import mta.k27.watermarking.util.dct.DecoderDctTask;

import static android.content.ContentValues.TAG;
import static mta.k27.watermarking.util.lsb.FileUtils.decodeBitmapScaledDown;

public class DCTDecodeActivity extends Activity {
    private static final String LOG_TAG = DCTEncodeActivity.class.getSimpleName();
    private File baseImage;
    private boolean isBase = true;
    private File secretFile;
    private File encodedTempImage;
    private ImageView baseView;
    private ImageView secretView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_decode_dct);

        ActionButton actionButton = (ActionButton) findViewById(R.id.fab_encode);
        actionButton.setImageResource(R.drawable.ic_arrow_forward_black_24dp);

        encodedTempImage = new File(getCacheDir(), "temp.png");
        baseView = (ImageView) findViewById(R.id.imageViewBase);
        secretView = (ImageView) findViewById(R.id.imageViewSecret);

    }

    public void uploadImage1(View v) {
        isBase = true;
        uploadImage();
    }

    public void uploadImage2(View v) {
        isBase = false;
        uploadFile();
    }

    public static final int SELECT_PICTURE = 1; //for the result listener
    public static final int SELECT_FILE = 2;
    private String selectedImagePath;

    private void uploadImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
    }

    private void uploadFile() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("*/*");
        startActivityForResult(Intent.createChooser(intent, "File"), SELECT_FILE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                Uri selectedImageUri = data.getData();
                selectedImagePath = getPath(selectedImageUri);
                baseImage = new File(selectedImagePath);
                baseView.setImageBitmap(decodeBitmapScaledDown(this, selectedImagePath));
            } else if (requestCode == SELECT_FILE) {
                Uri selectedSecretUri = data.getData();
                try {
                    Log.e(LOG_TAG, getPath(selectedSecretUri));
                    secretFile = new File(getPath(selectedSecretUri));
                } catch (Exception e) {
                    Log.e(LOG_TAG, "exception", e);
                }

                if (getApplicationContext().getContentResolver().getType(selectedSecretUri).contains("image")) {
                    secretView.setImageBitmap(decodeBitmapScaledDown(this, getPath(selectedSecretUri)));
                }
            }
        }
    }


    public String getPath(Uri uri) {
        // just some safety built in
        if (uri == null) {
            return null;
        }
        // try to retrieve the image from the media store first
        // this will only work for images selected from gallery
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        // this is our fallback here
        return uri.getPath();
    }


    public static final String EXTRA_FILE_TAG = "ENCODED FILE";

    public void encodeImage(View v) {
        if (baseImage != null) {
            try {
                ProgressDialog progress = new ProgressDialog(this);
                progress.setTitle("DCT Decoding");
                progress.setMessage("This may take a few minutes for large files...");
                progress.show();

                final DecoderDctTask decoderDctTask = new DecoderDctTask(this, progress);
                progress.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        Log.e(TAG, "progressBar onDismiss");
                        decoderDctTask.cancel(true);
                    }
                });
                decoderDctTask.execute(baseImage, secretFile);
            } catch (OutOfMemoryError e) {
                Toast toast = Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT);
                toast.show();
            }
        } else {
            Toast toast = Toast.makeText(getApplicationContext(), "You need two images", Toast.LENGTH_SHORT);
            toast.show();
        }
    }
}
