package mta.k27.watermarking.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.k27.watermarking.R;

public class MainActivity extends Activity {
    public static long startTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    finish();
                    startActivity(new Intent(this, MainActivity.class));
                } else {
                }
            }
        }

    }

    public void encode(View v) {
        startActivity(new Intent(this, EncodeActivity.class));
    }

    public void decode(View v) {
        startActivity(new Intent(this, DecodeActivity.class));
    }

    public void encodeDCT(View v) {
        startActivity(new Intent(this, DCTEncodeActivity.class));
    }

    public void decodeDCT(View v) {
        startActivity(new Intent(this, DCTDecodeActivity.class));
    }
}
