package mta.k27.watermarking.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.k27.watermarking.R;

import java.util.ArrayList;

import mta.k27.watermarking.util.lsb.EvalutionUtils;
import mta.k27.watermarking.util.lsb.FileUtils;

import static android.content.ContentValues.TAG;

public class ImageActivity extends Activity {
    private static final String LOG_TAG = ImageActivity.class.getSimpleName();
    public static final String TYPE = "TYPE";
    public static final int ENCODE = 1;
    public static final int DECODE = 2;
    public static final String SIG_DATA = "SIG_DATA";
    public static final String SIG_DECODED_DATA = "SIG_DECODED_DATA";
    public static final String IMAGE_DATA = "IMAGE_DATA";
    public static final String IMAGE_ENCODED_DATA = "IMAGE_ENCODED_DATA";
    public static final String IMAGE_WIDTH = "IMAGE_WIDTH";
    public static final String IMAGE_HEIGHT = "IMAGE_HEIGHT";
    public static final String IMAGE_NAME = "IMAGE_NAME";
    public static String dataSig;
    public static ArrayList<Integer> dataImg;
    public static ArrayList<Integer> dataImgEncoded;
    //    public static ArrayList<Integer> dataSigInt;
    public static ArrayList<Integer> dataBit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);
        init();
    }

    private void init() {
        Intent intent = getIntent();
        final String displayPath = intent.getStringExtra(EncodeActivity.EXTRA_FILE_TAG);
        final ImageView imageView = (ImageView) findViewById(R.id.imageView);
        new AsyncTask<Void, Void, Bitmap>() {
            @Override
            protected Bitmap doInBackground(Void... voids) {
                return FileUtils.decodeBitmapScaledDown(ImageActivity.this, displayPath);
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                super.onPostExecute(bitmap);
                imageView.setImageBitmap(bitmap);
            }
        }.execute();

        try {
            int cols = intent.getIntExtra(IMAGE_WIDTH, 0);
            int rows = intent.getIntExtra(IMAGE_HEIGHT, 0);
            String name = intent.getStringExtra(IMAGE_NAME);

            FileUtils.saveData(this, name, dataSig);
            double mse = EvalutionUtils.getMse(cols, rows, dataImg, dataImgEncoded);
            ((TextView) findViewById(R.id.tv_mse)).setText("Time = " + (System.currentTimeMillis() - MainActivity.startTime) / 1000);
            ((TextView) findViewById(R.id.tv_psnr)).setText("PSNR = " + EvalutionUtils.getPsnr(mse));
            Log.e(TAG, "MSE = " + mse);
            Log.e(TAG, "PSNR = " + EvalutionUtils.getPsnr(mse));
        } catch (Exception e) {
            Log.e("ImageActivity", "Exception: " + e.toString());
            e.printStackTrace();
        }

    }

}
