package mta.k27.watermarking.util.lsb;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

import mta.k27.watermarking.activity.EncodeActivity;
import mta.k27.watermarking.activity.ImageActivity;
import mta.k27.watermarking.activity.MainActivity;

import static mta.k27.watermarking.activity.ImageActivity.dataImg;
import static mta.k27.watermarking.activity.ImageActivity.dataImgEncoded;
import static mta.k27.watermarking.activity.ImageActivity.dataSig;


public class EncoderTask extends AsyncTask<String, Integer, String> {
    Boolean TEST = false;
    private static final String LOG_TAG = EncoderTask.class.getSimpleName();
    private Context context;
    private ProgressDialog progressBar;
    public static final int OVERHEAD_SIZE = 64;
    private int pixelRow;
    private int pixelCol;
    int bitCount = 0;
    int percent = -1;
    long lengLogo;
    int wImage, hImage;
    String nameImage;

    public EncoderTask(Context context, ProgressDialog progressBar) {
        this.context = context;
        this.progressBar = progressBar;
        dataSig = "";
        dataImg = new ArrayList<>();
        dataImgEncoded = new ArrayList<>();
        MainActivity.startTime = System.currentTimeMillis();
    }

    @Override
    protected String doInBackground(String... params) {
        pixelRow = 0;
        pixelCol = 0;
        String[] spl = params[0].split("/");
        nameImage = "LSB_" + spl[spl.length - 1].replace("jpg", "").replace("JPG", "");
        File folder = new File(Environment.getExternalStorageDirectory() + "/Watermarking");
        if (!folder.exists()) {
            folder.mkdir();
        }
        File exportFile = new File(folder.getPath() + "/" + nameImage + "png");
        try {
//            params[0] = FileUtils.convert(params[0], context.getCacheDir().getPath());
            Bitmap image = BitmapFactory.decodeFile(params[0]).copy(Bitmap.Config.ARGB_8888, true);
            Bitmap logo = BitmapFactory.decodeFile(params[1]).copy(Bitmap.Config.ARGB_8888, true);
//            BufferedInputStream stream = new BufferedInputStream(new FileInputStream(params[1]));

            lengLogo = logo.getWidth() * logo.getHeight() * 4;
            wImage = image.getWidth();
            hImage = image.getHeight();
            int numBitsPossible = wImage * hImage * 3;
            Log.e("EncoderDctTask", "numBitsPossible = " + numBitsPossible + " - need = " + ((lengLogo * 8) + 2 * OVERHEAD_SIZE));
            Log.e("logo", "getHeight = " + logo.getHeight() + " - getWidth = " + logo.getWidth());
            Log.e("EncoderDctTask", "nameImage = " + nameImage);
            if (numBitsPossible < ((lengLogo * 8) + 2 * OVERHEAD_SIZE)) {
                return null;
            }
            if (TEST) {
                for (int x = 0; x < 8; x++) {
                    for (int y = 0; y < 8; y++) {
                        int pixel = image.getPixel(x, y);
                        Log.e("IMAGE " + x + "," + y, Color.red(pixel) + " - " + Color.green(pixel) + " - " + Color.blue(pixel));
                    }
                }
                for (int y = 0; y < 6; y++) {
                    int pixel = image.getPixel(0, y);
                    Log.e("LOGO 0," + y, Color.alpha(pixel) + " - " + Color.red(pixel) + " - " + Color.green(pixel) + " - " + Color.blue(pixel));
                }
                return null;
            }
            dataImg.add((int) (0xFF & image.getPixel(0, 0)));

            hideLSB(image, logo.getWidth());
            hideLSB(image, logo.getHeight());
            for (int x = 0; x < logo.getWidth(); x++) {
                for (int y = 0; y < logo.getHeight(); y++) {
                    int pixel = logo.getPixel(x, y);
                    dataSig += Color.red(pixel) + " " + Color.green(pixel) + " " + Color.blue(pixel) + " ";
                    hideLSB(image, Color.alpha(pixel));
                    hideLSB(image, Color.red(pixel));
                    hideLSB(image, Color.green(pixel));
                    hideLSB(image, Color.blue(pixel));
                    if (isCancelled()) {
                        return null;
                    }
                }
            }
            dataImgEncoded.add((int) (0xFF & image.getPixel(pixelCol, pixelRow))); // lay ra pixel da encode

            exportFile.createNewFile();
            FileOutputStream out = new FileOutputStream(exportFile);
            image.compress(Bitmap.CompressFormat.PNG, 100, out);
            out.flush();
            out.close();
            image.recycle();
            publishProgress(100);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                final Intent scanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                final Uri contentUri = Uri.fromFile(exportFile);
                scanIntent.setData(contentUri);
                context.sendBroadcast(scanIntent);
            } else {
                context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory())));
            }
        } catch (OutOfMemoryError e) {
            throw new OutOfMemoryError("Not Enough RAM");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return exportFile.getPath();
    }

    private void hideLSB(Bitmap image, int currentByteInt) {
        byte currentByte = (byte) currentByteInt;
        dataSig += (int) (0xFF & currentByte) + " ";
        for (int j = 7; j >= 0; j--) {
            int bit = (currentByte & (0x1 << j)) >> j;
            bit = bit & 0x1;
            setPixelImage(image, bitCount, bit);
            bitCount++;
        }
        checkUpdate();
    }

    private void checkUpdate() {
        int per = (int) ((((double) bitCount) / ((double) (lengLogo * 8))) * 100);
        if (per > 99) {
            return;
        }
        if (per > percent) {
            publishProgress(per);
            percent = per;
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        progressBar.setTitle("Encoding " + values[0] + "%");
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String file) {
        progressBar.dismiss();
        if (file == null) {
            Toast.makeText(context, "The secret image bits number is bigger than the base image bits number used to hide", Toast.LENGTH_LONG).show();
        } else {
            Intent intent = new Intent(this.context, ImageActivity.class);
            intent.putExtra(EncodeActivity.EXTRA_FILE_TAG, file);
            intent.putExtra(ImageActivity.TYPE, ImageActivity.ENCODE);
//            intent.putExtra(ImageActivity.SIG_DATA, dataSig);
//            intent.putExtra(ImageActivity.IMAGE_DATA, dataImg);
//            intent.putExtra(ImageActivity.IMAGE_ENCODED_DATA, dataImgEncoded);
            intent.putExtra(ImageActivity.IMAGE_WIDTH, wImage);
            intent.putExtra(ImageActivity.IMAGE_HEIGHT, hImage);
            intent.putExtra(ImageActivity.IMAGE_NAME, nameImage);
            context.startActivity(intent);
            ((Activity) context).finish();
        }
    }

    private void setPixelImage(Bitmap image, int byteImageCount, int bit) {
        if (byteImageCount % 3 == 0) {
            int red;
            if (bit == 0) {
                red = Color.red(image.getPixel(pixelCol, pixelRow)) & 0xFE;
            } else {
                red = Color.red(image.getPixel(pixelCol, pixelRow)) | 0x1;
            }
            image.setPixel(pixelCol, pixelRow, Color.argb(
                    Color.alpha(image.getPixel(pixelCol, pixelRow)), red,
                    Color.green(image.getPixel(pixelCol, pixelRow)),
                    Color.blue(image.getPixel(pixelCol, pixelRow))));
        } else if (byteImageCount % 3 == 1) {
            int blue;
            if (bit == 0) {
                blue = Color.blue(image.getPixel(pixelCol, pixelRow)) & 0xFE;
            } else {
                blue = Color.blue(image.getPixel(pixelCol, pixelRow)) | 0x1;
            }
            image.setPixel(pixelCol, pixelRow, Color.argb(
                    Color.alpha(image.getPixel(pixelCol, pixelRow)),
                    Color.red(image.getPixel(pixelCol, pixelRow)),
                    Color.green(image.getPixel(pixelCol, pixelRow)), blue));
        } else {
            int green;
            if (bit == 0) {
                green = Color.green(image.getPixel(pixelCol, pixelRow)) & 0xFE;
            } else {
                green = Color.green(image.getPixel(pixelCol, pixelRow)) | 0x1;
            }
            image.setPixel(pixelCol, pixelRow, Color.argb(
                    Color.alpha(image.getPixel(pixelCol, pixelRow)),
                    Color.red(image.getPixel(pixelCol, pixelRow)), green,
                    Color.blue(image.getPixel(pixelCol, pixelRow))));
            incrementPixel(image);
        }
    }

    private void incrementPixel(Bitmap image) {
        dataImgEncoded.add(Color.red(image.getPixel(pixelCol, pixelRow))); // lay ra pixel da encode
        int length = image.getWidth();
        pixelCol++;
        if (pixelCol == length) {
            pixelCol = 0;
            pixelRow++;
        }
        dataImg.add(Color.red(image.getPixel(pixelCol, pixelRow))); // lay ra pixel nguyen ban
    }

}
