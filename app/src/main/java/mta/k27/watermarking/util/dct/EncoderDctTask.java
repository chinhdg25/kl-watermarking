package mta.k27.watermarking.util.dct;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

import mta.k27.watermarking.activity.EncodeActivity;
import mta.k27.watermarking.activity.ImageActivity;
import mta.k27.watermarking.activity.ImageDCTActivity;
import mta.k27.watermarking.activity.MainActivity;

import static mta.k27.watermarking.activity.ImageActivity.dataBit;
import static mta.k27.watermarking.activity.ImageActivity.dataImg;
import static mta.k27.watermarking.activity.ImageActivity.dataImgEncoded;
import static mta.k27.watermarking.activity.ImageActivity.dataSig;


public class EncoderDctTask extends AsyncTask<File, Integer, File> {
    public static boolean TEST = false;
    private static final String LOG_TAG = EncoderDctTask.class.getSimpleName();
    private Context context;
    private ProgressDialog progressBar;
    public static final int OVERHEAD_SIZE = 64;
    private int blockRow, blockCol, maxRow, maxCol;
    DCT dct;
    int bitCount = 0;
    int percent = -1;
    long numberBitNeeded;
    int wImage, hImage;
    String nameImage;
    public static double MIN_SPACE = 10;
    public static double RANK_TOP = 100;
    public static double RANK_BOT = 0;

    public EncoderDctTask(Context context, ProgressDialog progressBar) {
        this.context = context;
        this.progressBar = progressBar;
        this.progressBar.setCancelable(false);
        this.dct = new DCT();
        dataImg = new ArrayList<>();
        dataImgEncoded = new ArrayList<>();
//        dataSigInt = new ArrayList<>();
        dataBit = new ArrayList<>();
        MainActivity.startTime = System.currentTimeMillis();
    }

    @Override
    protected File doInBackground(File... params) {
        blockRow = 0;
        blockCol = 0;
        String[] spl = params[0].getPath().split("/");
        nameImage = "DCT_" + spl[spl.length - 1].replace("jpg", "").replace("JPG", "");
        File folder = new File(Environment.getExternalStorageDirectory() + "/Watermarking");
        if (!folder.exists()) {
            folder.mkdir();
        }
        File exportFile = new File(folder.getPath() + "/" + nameImage + "png");
        try {
            Bitmap image = BitmapFactory.decodeFile(params[0].getPath()).copy(Bitmap.Config.ARGB_8888, true);
            wImage = image.getWidth();
            hImage = image.getHeight();
            maxCol = image.getWidth() / 8;
            maxRow = image.getHeight() / 8;
            int numBitsPossible = maxCol * maxRow;

            Bitmap logo = BitmapFactory.decodeFile(params[1].getPath()).copy(Bitmap.Config.ARGB_8888, true);
            Log.e("EncoderDctTask", "numBitsPossible = " + numBitsPossible + " - need = " + ((logo.getWidth() * logo.getHeight() * 4 * 8) + 2 * OVERHEAD_SIZE));
            Log.e("logo", "getHeight = " + logo.getHeight() + " - getWidth = " + logo.getWidth());
            Log.e("EncoderDctTask", "nameImage = " + nameImage);
            numberBitNeeded = logo.getWidth() * logo.getHeight() * 4 * 8 + 2 * OVERHEAD_SIZE;
            if (numBitsPossible < numberBitNeeded) {
                return null;
            }

            hideDCT(image, logo.getWidth());
            hideDCT(image, logo.getHeight());
            if (TEST) {
                return null;
            }
            for (int x = 0; x < logo.getWidth(); x++) {
                for (int y = 0; y < logo.getHeight(); y++) {
                    int pixel = logo.getPixel(x, y);
                    dataSig += Color.red(pixel) + " " + Color.green(pixel) + " " + Color.blue(pixel) + " ";
                    hideDCT(image, Color.alpha(pixel));
                    hideDCT(image, Color.red(pixel));
                    hideDCT(image, Color.green(pixel));
                    hideDCT(image, Color.blue(pixel));
                    if (isCancelled()) {
                        return null;
                    }
                }
            }
            exportFile.createNewFile();
            FileOutputStream out = new FileOutputStream(exportFile);
            image.compress(Bitmap.CompressFormat.PNG, 100, out);
            out.flush();
            out.close();
            image.recycle();
            publishProgress(100);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                final Intent scanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                final Uri contentUri = Uri.fromFile(exportFile);
                scanIntent.setData(contentUri);
                context.sendBroadcast(scanIntent);
            } else {
                context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory())));
            }
        } catch (OutOfMemoryError e) {
            throw new OutOfMemoryError("Not Enough RAM");
        } catch (Exception e) {
            Log.e("max", "Exception e " + e.toString());
            e.printStackTrace();
        }
        return exportFile;
    }

    private void hideDCT(Bitmap image, int byt) {
        byte currentByte = (byte) byt;
        for (int j = 7; j >= 0; j--) {
            int bit = (currentByte & (0x1 << j)) >> j;
            bit = bit & 0x1;
            fdct(image, bit);
            bitCount++;
        }
        checkUpdate();
    }

    private void checkUpdate() {
        int per = (int) ((((double) bitCount) / ((double) numberBitNeeded)) * 100);
        if (per > 99) {
            return;
        }
        if (per > percent) {
            publishProgress(per);
            percent = per;
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        progressBar.setTitle("DCT Encoding " + values[0] + "%");
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(File file) {
        progressBar.dismiss();
        if (file == null) {
            Toast.makeText(context, "The secret image bits number is bigger than the base image bits number used to hide", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(context, "Success", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(this.context, ImageDCTActivity.class);
            intent.putExtra(EncodeActivity.EXTRA_FILE_TAG, file.getPath());
            intent.putExtra(ImageActivity.TYPE, ImageActivity.ENCODE);
            intent.putExtra(ImageActivity.IMAGE_WIDTH, wImage);
            intent.putExtra(ImageActivity.IMAGE_HEIGHT, hImage);
            intent.putExtra(ImageActivity.IMAGE_NAME, nameImage);
            context.startActivity(intent);
            ((Activity) context).finish();
        }
    }

    private double calculateAverage(double[][] block) {
        double sum = 0;
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                sum += block[i][j];
            }
        }
        return sum / 64;
    }

    private void fdct(Bitmap image, int bit) {
        dataBit.add(bit);
        double[][] block = getBlockImage(image);
        double[][] blockDct = dct.applyDCT(block);
        /**
         * blockDct[1][0] > blockDct[0][1] => bit 1
         * blockDct[1][0] <= blockDct[0][1] => bit 0
         */
        double avg = calculateAverage(blockDct);
        if (bit == 0) {
            if (blockDct[1][0] > blockDct[0][1]) {
                // bit = 1 => bit = 0
                if (Math.abs(blockDct[0][1] - avg) > Math.abs(blockDct[1][0] - avg)) {
                    double top = blockDct[0][1];
                    double bot = top - Math.abs(avg);
                    blockDct[1][0] = bot;
                } else {
                    double bot = blockDct[1][0];
                    double top = bot + Math.abs(avg);
                    blockDct[0][1] = top;
                }
            } else {
                // bit = 0 => true
//                incrementPixel();
//                return;
                if (Math.abs(blockDct[0][1] - blockDct[1][0]) < MIN_SPACE) {
                    if (Math.abs(blockDct[0][1] - avg) > Math.abs(blockDct[1][0] - avg)) {
                        blockDct[1][0] = blockDct[1][0] - Math.abs(avg);
                    } else {
                        blockDct[0][1] = blockDct[0][1] + Math.abs(avg);
                    }
                }
            }
        } else {
            if (blockDct[1][0] > blockDct[0][1]) {
                // bit = 1 => true
//                incrementPixel();
//                return;
                if (Math.abs(blockDct[1][0] - blockDct[0][1]) < MIN_SPACE) {
                    if (Math.abs(blockDct[0][1] - avg) > Math.abs(blockDct[1][0] - avg)) {
                        blockDct[1][0] = blockDct[1][0] + Math.abs(avg);
                    } else {
                        blockDct[0][1] = blockDct[0][1] - Math.abs(avg);
                    }
                }
            } else {
                // bit = 0 => bit 1
                if (Math.abs(blockDct[0][1] - avg) > Math.abs(blockDct[1][0] - avg)) {
                    double bot = blockDct[0][1];
                    double top = bot + Math.abs(avg);
                    blockDct[1][0] = top;
                } else {
                    double top = blockDct[1][0];
                    double bot = top - Math.abs(avg);
                    blockDct[0][1] = bot;
                }
            }
        }
        double[][] blockDctInverse = dct.applyIDCT(blockDct);

        setBlockImage(image, blockDctInverse);
        incrementPixel();
    }

    private double[][] getBlockImage(Bitmap bitmap) {
        double[][] block = new double[8][8];
        int x = blockCol * 8;
        int y = blockRow * 8;
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                block[i][j] = getColorPixel(bitmap, x + i, y + j);
            }
        }
        return block;
    }

    private void setBlockImage(Bitmap bitmap, double[][] block) {
        int x = blockCol * 8;
        int y = blockRow * 8;
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                setColorPixel(bitmap, x + i, y + j, block[i][j]);
            }
        }
    }

    private double getColorPixel(Bitmap bitmap, int x, int y) {
        int pixel = bitmap.getPixel(x, y);
        int color = Color.blue(pixel);
        dataImg.add(color);
        return (double) (color);
    }

    private void setColorPixel(Bitmap bitmap, int x, int y, double color) {
        int pixel = bitmap.getPixel(x, y);
        bitmap.setPixel(x, y, Color.argb(Color.alpha(pixel), Color.red(pixel), Color.green(pixel), (int) color));
        dataImgEncoded.add((int) color);
    }

    private void incrementPixel() {
        blockCol++;
        if (blockCol == maxCol) {
            blockCol = 0;
            blockRow++;
        }
    }

}
