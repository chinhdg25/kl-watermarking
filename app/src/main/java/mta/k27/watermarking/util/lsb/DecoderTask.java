package mta.k27.watermarking.util.lsb;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import mta.k27.watermarking.activity.EncodeActivity;
import mta.k27.watermarking.activity.ImageActivity;
import mta.k27.watermarking.activity.ImageDecodedActivity;
import mta.k27.watermarking.activity.MainActivity;

import static android.content.ContentValues.TAG;

public class DecoderTask extends AsyncTask<File, Integer, File> {
    private static final String LOG_TAG = DecoderTask.class.getSimpleName();
    private Context context;
    private ProgressDialog progressBar;
    private int pixelRow;
    private int pixelCol;
    public static ArrayList<Integer> dataDecodedSig;
    public static ArrayList<Integer> dataSig;
    int bitCount = 0;
    int percent = -1;
    long widthLogo = 0x0;
    long heightLogo = 0x0;

    public DecoderTask(Context context, ProgressDialog progressBar) {
        this.context = context;
        this.progressBar = progressBar;
        dataDecodedSig = new ArrayList<>();
        dataSig = new ArrayList<>();
        MainActivity.startTime = System.currentTimeMillis();
    }

    @Override
    protected File doInBackground(File... params) {
        return decode(params);
    }

    private File decode(File... params) {
        pixelCol = 0;
        pixelRow = 0;
        String[] spl = params[0].getPath().split("/");
        String nameImage = "sig_" + "LSB_" + spl[spl.length - 1].replace("jpg", "").replace("JPG", "").replace("png", "").replace("PNG", "");
        File folder = new File(Environment.getExternalStorageDirectory() + "/Watermarking");
        if (!folder.exists()) {
            folder.mkdir();
        }
        File exportFile = new File(folder.getPath() + "/" + nameImage + "png");

        try {
            Log.e(TAG, "start decode");
            exportFile.createNewFile();
            Bitmap imageEncoded = BitmapFactory.decodeFile(params[0].getPath()).copy(Bitmap.Config.ARGB_8888, true);

            widthLogo = getLSB(imageEncoded);
            heightLogo = getLSB(imageEncoded);
            try {
                if (params[1] != null) {
                    Bitmap logo = BitmapFactory.decodeFile(params[1].getPath()).copy(Bitmap.Config.ARGB_8888, true);
                    if (logo != null) {
                        widthLogo = logo.getWidth();
                        heightLogo = logo.getHeight();
                        for (int x = 0; x < widthLogo; x++) {
                            for (int y = 0; y < heightLogo; y++) {
                                int pixel = logo.getPixel(x, y);
                                dataSig.add(Color.red(pixel));
                                dataSig.add(Color.green(pixel));
                                dataSig.add(Color.blue(pixel));
                            }
                        }
                        logo.recycle();
                    }
                }
            } catch (Exception e) {
            }
            Log.e(TAG, "widthLogo = " + widthLogo);
            Log.e(TAG, "heightLogo = " + heightLogo);
            Bitmap bmpLogo = Bitmap.createBitmap((int) widthLogo, (int) heightLogo, Bitmap.Config.ARGB_8888);
            for (int x = 0; x < widthLogo; x++) {
                for (int y = 0; y < heightLogo; y++) {
//                Log.e(TAG, "widthLogo = " + i);
                    if (isCancelled()) {
                        return null;
                    }
                    int alpha = getLSB(imageEncoded);
                    int red = getLSB(imageEncoded);
                    int green = getLSB(imageEncoded);
                    int blue = getLSB(imageEncoded);
                    dataDecodedSig.add(red);
                    dataDecodedSig.add(green);
                    dataDecodedSig.add(blue);

                    bmpLogo.setPixel(x, y, Color.argb(alpha, red, green, blue));
                    checkUpdate();
                }
            }
            FileOutputStream out = new FileOutputStream(exportFile);
            bmpLogo.compress(Bitmap.CompressFormat.PNG, 100, out);
            out.flush();
            out.close();
            bmpLogo.recycle();

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                final Intent scanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                final Uri contentUri = Uri.fromFile(exportFile);
                scanIntent.setData(contentUri);
                context.sendBroadcast(scanIntent);
            } else {
                context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory())));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return exportFile;
    }

    private int getLSB(Bitmap imageEncoded) {
        int current = 0;
        for (int j = 7; j >= 0; j--) {
            if (bitCount % 3 == 0) {
                if ((Color.red(imageEncoded.getPixel(pixelCol, pixelRow)) & 0x1) == 0x1) {
                    current = current | (0x1 << j);
                }
            } else if (bitCount % 3 == 1) {
                if ((Color.blue(imageEncoded.getPixel(pixelCol, pixelRow)) & 0x1) == 0x1) {
                    current = current | (0x1 << j);
                }
            } else {
                if ((Color.green(imageEncoded.getPixel(pixelCol, pixelRow)) & 0x1) == 0x1) {
                    current = current | (0x1 << j);
                }
                incrementPixel(imageEncoded.getWidth());
            }
            bitCount++;
        }
        checkUpdate();
        return current;
    }

    private void checkUpdate() {
        int per = (int) ((((double) bitCount) / ((double) (widthLogo * 8))) * 100);
        if (per > 99) {
            return;
        }
        if (per > percent) {
            publishProgress(per);
            percent = per;
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        progressBar.setTitle("Decoding " + values[0] + "%");
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(File file) {
        progressBar.dismiss();
        Intent intent = new Intent(this.context, ImageDecodedActivity.class);
        intent.putExtra(ImageActivity.IMAGE_WIDTH, widthLogo);
        intent.putExtra(ImageActivity.IMAGE_HEIGHT, heightLogo);
        intent.putExtra(EncodeActivity.EXTRA_FILE_TAG, file.getPath());
        intent.putExtra(ImageActivity.TYPE, ImageActivity.DECODE);
        context.startActivity(intent);
        ((Activity) context).finish();
    }

    private void incrementPixel(int length) {
        pixelCol++;
        if (pixelCol == length) {
            pixelCol = 0;
            pixelRow++;
        }
    }
}
