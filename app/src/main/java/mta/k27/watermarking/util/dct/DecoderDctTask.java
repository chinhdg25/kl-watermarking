package mta.k27.watermarking.util.dct;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

import mta.k27.watermarking.activity.EncodeActivity;
import mta.k27.watermarking.activity.ImageActivity;
import mta.k27.watermarking.activity.ImageDecodedActivity;
import mta.k27.watermarking.activity.MainActivity;

import static android.content.ContentValues.TAG;
import static mta.k27.watermarking.util.lsb.DecoderTask.dataDecodedSig;
import static mta.k27.watermarking.util.lsb.DecoderTask.dataSig;

public class DecoderDctTask extends AsyncTask<File, Integer, File> {
    private static final String LOG_TAG = DecoderDctTask.class.getSimpleName();
    private Context context;
    private ProgressDialog progressBar;
    private int blockRow, blockCol, maxCol;
    DCT dct;
    int bitCount = 0;
    int percent = -1;
    long widthLogo = 0x0;
    long heightLogo = 0x0;
    String nameImage;
    int count = 0;

    public DecoderDctTask(Context context, ProgressDialog progressBar) {
        this.context = context;
        this.progressBar = progressBar;
        this.progressBar.setCancelable(false);
        this.dct = new DCT();
        dataDecodedSig = new ArrayList<>();
        dataSig = new ArrayList<>();
        MainActivity.startTime = System.currentTimeMillis();
    }

    @Override
    protected File doInBackground(File... params) {
        return decode(params);
    }

    private File decode(File... params) {
        blockCol = 0;
        blockRow = 0;
        String[] spl = params[0].getPath().split("/");
        nameImage = "sig_" + "LSB_" + spl[spl.length - 1].replace("jpg", "").replace("JPG", "").replace("png", "").replace("PNG", "");
        File folder = new File(Environment.getExternalStorageDirectory() + "/Watermarking");
        if (!folder.exists()) {
            folder.mkdir();
        }
        File exportFile = new File(folder.getPath() + "/" + nameImage + "png");

        try {
            Log.e(TAG, "start decode");
            exportFile.createNewFile();

            Bitmap imageEncoded = BitmapFactory.decodeFile(params[0].getPath()).copy(Bitmap.Config.ARGB_8888, true);
            maxCol = imageEncoded.getWidth() / 8;
            Log.e("DecoderDctTask", "numBitsPossible = " + (imageEncoded.getWidth() / 8) * (imageEncoded.getHeight() / 8));
            widthLogo = getValuePixel(imageEncoded);
            heightLogo = getValuePixel(imageEncoded);
            try {
                if (params[1] != null) {
                    Bitmap logo = BitmapFactory.decodeFile(params[1].getPath()).copy(Bitmap.Config.ARGB_8888, true);
                    if (logo != null) {
                        widthLogo = logo.getWidth();
                        heightLogo = logo.getHeight();
                        for (int x = 0; x < widthLogo; x++) {
                            for (int y = 0; y < heightLogo; y++) {
                                int pixel = logo.getPixel(x, y);
                                dataSig.add(Color.red(pixel));
                                dataSig.add(Color.green(pixel));
                                dataSig.add(Color.blue(pixel));
                            }
                        }
                        logo.recycle();
                    }
                }
            } catch (Exception e) {
            }
            Log.e("widthLogo", "widthLogo = " + widthLogo);
            Log.e("heightLogo", "heightLogo = " + heightLogo);
            Bitmap bmpLogo = Bitmap.createBitmap((int) widthLogo, (int) heightLogo, Bitmap.Config.ARGB_8888);
            for (int x = 0; x < widthLogo; x++) {
                for (int y = 0; y < heightLogo; y++) {
//                Log.e(TAG, "widthLogo = " + i);
                    if (isCancelled()) {
                        return null;
                    }
                    int alpha = getValuePixel(imageEncoded);
                    int red = getValuePixel(imageEncoded);
                    int green = getValuePixel(imageEncoded);
                    int blue = getValuePixel(imageEncoded);
                    dataDecodedSig.add(red);
                    dataDecodedSig.add(green);
                    dataDecodedSig.add(blue);
                    bmpLogo.setPixel(x, y, Color.argb(alpha, red, green, blue));
                    checkUpdate();
                }
            }
            FileOutputStream out = new FileOutputStream(exportFile);
            bmpLogo.compress(Bitmap.CompressFormat.PNG, 100, out);
            out.flush();
            out.close();
            bmpLogo.recycle();

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                final Intent scanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                final Uri contentUri = Uri.fromFile(exportFile);
                scanIntent.setData(contentUri);
                context.sendBroadcast(scanIntent);
            } else {
                context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory())));
            }
        } catch (FileNotFoundException e) {
            Log.e("Exception", "Exception: " + e.toString());
            e.printStackTrace();
        } catch (Exception e) {
            Log.e("Exception", "Exception: " + e.toString());
            e.printStackTrace();
        }
        return exportFile;
    }

    private int getValuePixel(Bitmap imageEncoded) {
        int current = 0;
        for (int j = 7; j >= 0; j--) {
            int bit = getBit(imageEncoded);
            if (bit == 1) {
                current = current | (0x1 << j);
            }
            bitCount++;
        }
        if (current < 0) {
            current = 256 + current;
        }
        return current;
    }

    private int getBit(Bitmap image) {
        double[][] block = getBlockImage(image);
        double[][] blockDct = dct.applyDCT(block);
        incrementPixel();
        if (blockDct[1][0] > blockDct[0][1]) {
            return 1;
        } else {
            return 0;
        }
    }

    private double[][] getBlockImage(Bitmap bitmap) {
        double[][] block = new double[8][8];
        int x = blockCol * 8;
        int y = blockRow * 8;
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                int pixel = bitmap.getPixel(x + i, y + j);
//                Log.e("getBlockImage", (dataImgEncoded.get(count) == pixel) + ", dataImgEncoded = " + dataImgEncoded.get(count) + ", pixel = " + pixel);
                count++;
                double red = (double) (Color.blue(pixel));
                block[i][j] = red;
            }
        }
        return block;
    }

    private void checkUpdate() {
        int per = (int) ((((double) bitCount) / ((double) (widthLogo * heightLogo * 4 * 8))) * 100);
        if (per > 99) {
            return;
        }
        if (per > percent) {
            publishProgress(per);
            percent = per;
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        progressBar.setTitle("DCT Decoding " + values[0] + "%");
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(File file) {
        progressBar.dismiss();
        if (file == null) {
            return;
        }
        Log.e("DecoderDctTask", "file.getPath(): " + file.getPath());
        Intent intent = new Intent(this.context, ImageDecodedActivity.class);
        intent.putExtra(ImageActivity.IMAGE_WIDTH, widthLogo);
        intent.putExtra(ImageActivity.IMAGE_HEIGHT, heightLogo);
        intent.putExtra(EncodeActivity.EXTRA_FILE_TAG, file.getPath());
        intent.putExtra(ImageActivity.TYPE, ImageActivity.DECODE);
        intent.putExtra(ImageActivity.SIG_DATA, dataSig);
        intent.putExtra(ImageActivity.SIG_DECODED_DATA, dataDecodedSig);
        context.startActivity(intent);
        ((Activity) context).finish();
    }

    private void incrementPixel() {
        blockCol++;
        if (blockCol == maxCol) {
            blockCol = 0;
            blockRow++;
        }
    }
}
