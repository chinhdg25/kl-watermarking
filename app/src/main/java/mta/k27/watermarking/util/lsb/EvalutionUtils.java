package mta.k27.watermarking.util.lsb;

import android.util.Log;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;

/**
 * Created by ChinhDH on 20/04/2017.
 * ********************************************************************
 * ********************************************************************
 */

public class EvalutionUtils {

    public static double log10(double x) {
        return Math.log(x) / Math.log(10);
    }

    // Computes the MSE between two images.
    public static double getMse(int nrows, int ncols, ArrayList<Integer> imgData1, ArrayList<Integer> imgData2) {
        Boolean isFalse = true;
        double noise = 0, mse;
        for (int i = 0; i < imgData1.size(); i++) {
            try {
                int img1 = imgData1.get(i);
                int img2 = imgData2.get(i);
                if (img1 >= 0 && img2 >= 0) {
                    noise += (img1 - img2) * (img1 - img2);
                    isFalse = false;
                }
            } catch (Exception e) {
            }
        }
        if (isFalse) {
            return -1;
        }
        Log.e(TAG, "noise = " + noise);
        mse = noise / (nrows * ncols); // Mean square error
        return mse;
    }

    // Computes the MSE between two images.
    public static double getMse(int nrows, int ncols, String imgData1, String imgData2) {
        Boolean isFalse = true;
        double noise = 0, mse;
        String[] sp1 = imgData1.split(" ");
        String[] sp2 = imgData2.split(" ");
        for (int i = 0; i < sp1.length; i++) {
            int img1 = -1;
            int img2 = -1;
            try {
                img1 = Integer.parseInt(sp1[i]);
                img2 = Integer.parseInt(sp2[i]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (img1 >= 0 && img2 >= 0) {
                noise += (img1 - img2) * (img1 - img2);
                isFalse = false;
            }
        }
        if (isFalse) {
            return -1;
        }
        Log.e(TAG, "noise = " + noise);
        mse = noise / (nrows * ncols); // Mean square error
        return mse;
    }

    // Computes the PSNR between two images.
    public static double getPsnr(double mse) {
        return (10 * log10(255 * 255 / mse));
    }

    // Computes the NCC between two images.
    public static double getNCC(ArrayList<Integer> img1, ArrayList<Integer> img2) {
//        Boolean isFalse = true;
//        ArrayList<Integer> img1 = new ArrayList<>();
//        ArrayList<Integer> img2 = new ArrayList<>();
//        for (int i = 0; i < imgData1.size(); i++) {
//            int bit1 = imgData1.get(i);
//            int bit2 = imgData2.get(i);
//            if (bit1 >= 0 && bit2 >= 0) {
//                img1.add(bit1);
//                img2.add(bit2);
//                isFalse = false;
//            }
//        }
//        if (isFalse) {
//            return -1;
//        }
        double avg1 = calculateAverage(img1);
        double avg2 = calculateAverage(img2);
        double sd1 = calculateStandardDeviation(img1, avg1);
        double sd2 = calculateStandardDeviation(img2, avg2);
        return calculateNormalizedCrossCorrelation(img1, img2, avg1, avg2, sd1, sd2);
    }

    // Computes the NCC between two images.
    public static double getNCC(String imgData1, String imgData2) {
        Boolean isFalse = true;
        ArrayList<Integer> img1 = new ArrayList<>();
        ArrayList<Integer> img2 = new ArrayList<>();
        String[] sp1 = imgData1.split(" ");
        String[] sp2 = imgData2.split(" ");
        for (int i = 0; i < sp1.length; i++) {
            int bit1 = -1;
            int bit2 = -1;
            try {
                bit1 = Integer.parseInt(sp1[i]);
                bit2 = Integer.parseInt(sp2[i]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (bit1 >= 0 && bit2 >= 0) {
                img1.add(bit1);
                img2.add(bit2);
                isFalse = false;
            }
        }
        if (isFalse) {
            return -1;
        }
        double avg1 = calculateAverage(img1);
        double avg2 = calculateAverage(img2);
        double sd1 = calculateStandardDeviation(img1, avg1);
        double sd2 = calculateStandardDeviation(img2, avg2);
        return calculateNormalizedCrossCorrelation(img1, img2, avg1, avg2, sd1, sd2);
    }

    private static double calculateNormalizedCrossCorrelation(ArrayList<Integer> img1, ArrayList<Integer> img2, double avg1, double avg2, double sd1, double sd2) {
        double sum = 0;
        if (!img1.isEmpty()) {
            for (int i = 0; i < img1.size(); i++) {
                sum += ((img1.get(i) - avg1) * (img2.get(i) - avg2)) / (sd1 * sd2);
            }
            return sum / img1.size();
        }
        return sum;
    }

    private static double calculateStandardDeviation(ArrayList<Integer> marks, double avg) {
        double sum = 0;
        if (!marks.isEmpty()) {
            for (Integer mark : marks) {
                sum += (mark - avg) * (mark - avg);
            }
            sum = sum / marks.size();
            return Math.sqrt(sum);
        }
        return sum;
    }

    private static double calculateAverage(ArrayList<Integer> marks) {
        Integer sum = 0;
        if (!marks.isEmpty()) {
            for (Integer mark : marks) {
                sum += mark;
            }
            return sum.doubleValue() / marks.size();
        }
        return sum;
    }
}
