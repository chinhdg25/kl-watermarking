package mta.k27.watermarking.util.dct;


/**
 * Created by ChinhDH on 22/05/2017.
 */

public class DCT {
    private int N = 8;
    private double[] c;

    public DCT() {
        this.initializeCoefficients();
    }

    private void initializeCoefficients() {
        c = new double[N];
        for (int i = 1; i < N; i++) {
            c[i] = 1;
        }
        c[0] = 1 / Math.sqrt(2.0);
    }

    public double[][] applyDCT(double[][] f) {
        double[][] F = new double[N][N];
        for (int u = 0; u < N; u++) {
            for (int v = 0; v < N; v++) {
                double sum = 0.0;
                for (int i = 0; i < N; i++) {
                    for (int j = 0; j < N; j++) {
                        sum += Math.cos(((2 * i + 1) / (2.0 * N)) * u * Math.PI) * Math.cos(((2 * j + 1) / (2.0 * N)) * v * Math.PI) * f[i][j];
                    }
                }
//                works for a block 8x8
                sum *= ((c[u] * c[v]) / 4.0);
//                works for NxN
//                sum *= ((2 * c[u] * c[v]) / Math.sqrt(N * N));
                F[u][v] = sum;
            }
        }
        return F;
    }

    public double[][] applyIDCT(double[][] F) {
        double[][] f = new double[N][N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                double sum = 0.0;
                for (int u = 0; u < N; u++) {
                    for (int v = 0; v < N; v++) {
//                        works for a block 8x8
                        sum += (c[u] * c[v]) / 4.0 * Math.cos(((2 * i + 1) / (2.0 * N)) * u * Math.PI) * Math.cos(((2 * j + 1) / (2.0 * N)) * v * Math.PI) * F[u][v];

//                        works for NxN
//                        sum += ((2 * c[u] * c[v]) / Math.sqrt(N * N)) * Math.cos(((2 * i + 1) / (2.0 * N)) * u * Math.PI) * Math.cos(((2 * j + 1) / (2.0 * N)) * v * Math.PI) * F[u][v];
                    }
                }
                if (sum < 0) {
                    sum = 0;
                } else if (sum > 255) {
                    sum = 255;
                }
                f[i][j] = Math.round(sum);
            }
        }
        return f;
    }

}
